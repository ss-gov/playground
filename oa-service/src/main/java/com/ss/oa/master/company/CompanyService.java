package com.ss.oa.master.company;
 
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ss.oa.master.company.so.*;


@RestController
public class CompanyService {
	private ApplicationContext daoContext;
	

	public CompanyService() {
		super(); 
	}

	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/companies", method = RequestMethod.GET)
	public List<com.ss.oa.master.company.so.Company> getCompanies(
																@RequestParam(name="id",required=false) String id) {	
		
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("id", id);

		
		System.out.println("criteria-->"+criteria.toString());
		
		
		Company company = new Company();
		company.setId("123");
		company.setCompanyId("CMP-123");
		
		
		company.setService(new Service());
		company.getService().setId("sr-123");
		@SuppressWarnings("unchecked")
		List<Company> companies = new ArrayList<Company>();
		companies.add(company);
		
		/* 
		 * this code is needed when we have to encode the payload
		 * 
		byte[] encodedBytes = Base64Utils.encode("Test".getBytes());
		System.out.println("encodedBytes " + new String(encodedBytes));
		byte[] decodedBytes = Base64Utils.decode(encodedBytes);
		System.out.println("decodedBytes " + new String(decodedBytes));
		*/
		return companies;

	} 

	
}
