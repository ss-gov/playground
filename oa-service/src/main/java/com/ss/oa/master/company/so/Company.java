package com.ss.oa.master.company.so;

public class Company{
	private String id;
	private String companyId;
	private Service service;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", companyId=" + companyId + ", service=" + service + ", getId()=" + getId()
				+ ", getCompanyId()=" + getCompanyId() + ", getService()=" + getService() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
	

	
}

