package org.varun.firtstproject.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Student {
	long id;
	String firstname;
	String lastname;
	public Student()
	{
		
	}
	public Student(long id,String firstname,String lastname)
	{
		this.id=id;
		this.firstname=firstname;
		this.lastname=lastname;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + "]";
	}

	
}
