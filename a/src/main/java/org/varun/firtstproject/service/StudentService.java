package org.varun.firtstproject.service;
import java.awt.List;
import java.util.ArrayList;
import java.util.Map;
import org.varun.firtstproject.database.Database;
import org.varun.firtstproject.model.Student;

public class StudentService {
	public Map<Long,Student> students=Database.getStudents();
	public StudentService()
	{
	}
	public ArrayList<Student> getAllStudents()
	{
		return new ArrayList<Student>(students.values()); 
	}
	public Student getStudent(long id)
	{
		return students.get(id);
	}
	public Student addStudent(Student student)
	{
		student.setId(students.size()+1);
		students.put(student.getId(),student);
		return student;
	}
	public Student updateStudent(Student student)
	{
		if(student.getId()<0)
		{
			return null;
		}
		students.put(student.getId(),student);
		return student;
	}
	public void deleteStudent(long id)
	{
		students.remove(id);
	}
}