package org.varun.firtstproject.resource;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.varun.firtstproject.model.Student;
import org.varun.firtstproject.service.StudentService;



/**
 * @author varun
 * StudentResource will expose StudenService as an rest api
 * it will support following
 * getStudents()
 * addSTud
 */
@Path("student")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StudentResource {
	
	StudentService studentservice=new StudentService();
	@GET
	public ArrayList<Student> getStudents()
	{
		
		return studentservice.getAllStudents();
	}
	@GET
	@Path("/{studentId}")
	public Student getStudent(@PathParam("studentId") long id)
	{
		return studentservice.getStudent(id);
	}
	@POST
	public Student addStudent(Student student)
	{
		
		return studentservice.addStudent(student);
	}
	@PUT
	@Path("/{studentId}")
	public Student updateStudent(@PathParam("studentId") long id,Student student)
	{
		student.setId(id);
		return studentservice.updateStudent(student);
	}
	@DELETE
	@Path("/{studentId}")
	public void deletestudent(@PathParam("studentId") long id)
	{
		studentservice.deleteStudent(id);
	}
}
